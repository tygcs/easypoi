package cn.afterturn.easypoi.excel.chart;

import lombok.Data;

@Data
public class Option {
    /**
     * 全图默认背景，（详见backgroundColor），默认为无，透明
     */
    private String backgroundColor;
    /**
     * 数值系列的颜色列表，（详见color），可配数组，eg：['#87cefa', 'rgba(123,123,123,0.5)','...']，当系列数量个数比颜色列表长度大时将循环选取
     */
    private String color;
    /**
     * 是否开启动画，默认开启
     */
    private boolean animation = false;
    /**
     * 时间轴（详见timeline），每个图表最多仅有一个时间轴控件
     */
    private Object timeline;
    /**
     * 标题（详见title），每个图表最多仅有一个标题控件
     */
    private Object title;
    /**
     * 图例（详见legend），每个图表最多仅有一个图例，混搭图表共享
     */
    private Object legend;
    /**
     * 值域选择（详见dataRange）,值域范围
     */
    private Object dataRange;
    /**
     * 数据区域缩放（详见dataZoom）,数据展现范围选择
     */
    private Object dataZoom;
    /**
     * 直角坐标系内绘图网格（详见grid）
     */
    private Object grid;
    /**
     * 直角坐标系中横轴数组（详见xAxis），数组中每一项代表一条横轴坐标轴，标准（1.0）中规定最多同时存在2条横轴
     */
    private Object xAxis;
    /**
     * 直角坐标系中纵轴数组（详见yAxis），数组中每一项代表一条纵轴坐标轴，标准（1.0）中规定最多同时存在2条纵轴
     */
    private Object yAxis;
    /**
     * 驱动图表生成的数据内容（详见series），数组中每一项代表一个系列的特殊选项及数据
     */
    private Object series;


}
